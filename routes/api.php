<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {

    Route::apiResource('/users', 'Api\UsersController');
    Route::apiResource('/posts', 'Api\PostsController');
    Route::apiResource('/categories', 'Api\CategoriesController');
    Route::apiResource('/posts/{post}/comments', 'Api\CommentsController');
    Route::post('/like/{post}', 'Api\LikesController@like');
    Route::delete('/like/{post}', 'Api\LikesController@dislike');

});

Route::post('register', 'Auth\AuthController@registerUser');
Route::post('login', 'Auth\AuthController@userLogin');
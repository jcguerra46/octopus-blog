export default class Gate {

    constructor(user) {
        this.user = user;
    }

    isAdmin() {
        return this.user.role_id === 1;
    }

    isAuthor() {
        return this.user.role_id === 2;
    }

    isRegistered() {
        return this.user.role_id === 3;
    }

}
require('./bootstrap');
window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);
import routes from './routes';

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Passport
Vue.component('passport-clients', require('./components/passport/Clients.vue').default );
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default );
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default );

import Gate from "./gate";
Vue.prototype.$gate = new Gate(window.user);

import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import Vuetify from '../plugins/vuetify';
// import "vuetify/dist/vuetify.css";
// import 'material-design-icons-iconfont/dist/material-design-icons.css'

window.Fire = new Vue();

const app = new Vue({
    el: '#app',
    vuetify: Vuetify,
    router: new VueRouter(routes),
});

import Dashboard from './components/Dashboard';
import Users from './components/Users';
import Comments from './components/Comments';
import Categories from './components/Categories';
import Posts from './components/Posts';

export default {
    mode: 'history',
    routes: [
        { path: '/dashboard', component: Dashboard },
        { path: '/users', component: Users },
        { path: '/comments', component: Comments },
        { path: '/categories', component: Categories },
        { path: '/posts', component: Posts },
    ]

}
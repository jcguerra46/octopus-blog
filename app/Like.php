<?php

namespace App;

use App\User;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Like extends Model
{
    use HybridRelations;

    /**
     * Connection with MySQL DB
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @var string
     */
    protected $table = 'likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'post_id',
        'status',
    ];

//    /**
//     * Relation polymorphic with Post and Comment models.
//     *
//     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
//     */
//    public function likeable()
//    {
//        return $this->morphTo();
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

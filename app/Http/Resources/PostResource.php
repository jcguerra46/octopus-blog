<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'author' => $this->user->name,
            'category' => $this->category->name,
            'title' => $this->title,
            'slug' => $this->slug,
            'body' => $this->body,
            'sumary' => $this->sumary,
            'is_published' => $this->is_published,
            'path' => $this->path,
            'created_at' => $this->created_at->diffForHumans()

        ];
    }
}

<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{


    /**
     * Register User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function registerUser(Request $request)
    {
        $rules = $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|alpha_num|min:5',
            'confirm_password' => 'required|same:password',
            'role_id' => 'required|integer'
        ]);
//        if($rules->fails()) {
//            return response()->json(['Validation errors' => $rules->errors()]);
//        }
        $user = User::where('email', $request->email)->first();
        if(!is_null($user)) {
            $data['message'] = "Sorry! this email is already registered";
            return response()->json(['success' => false, 'status' => 'failed', 'data' => $data]);
        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id' => $request->role_id
        ]);
        $success['message'] = "You have registered successfully";
        return response()->json( [ 'success' => true, 'user' => $user ] );
    }

    /**
     * Login user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userLogin(Request $request)
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $token = $user->createToken('token')->accessToken;
            $success['success'] = true;
            $success['message'] = "Success! you are logged in successfully";
            $success['token'] = $token;
            return response()->json(['success' => $success ], Response::HTTP_OK);
        } else {
            return response()->json(['error'=>'Unauthorised'], Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){

            $token = User::whereEmail($request->email)
                ->first()
                ->createToken($request->email)
                ->accessToken;
            return response()->json(['token' => $token]);
        }
    }
}
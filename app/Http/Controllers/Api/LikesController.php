<?php

namespace App\Http\Controllers\Api;

use App\Post;
use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LikesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function like(Post $post)
    {
        $post->like()->create([
//            'user_id' => 1
            'user_id' => auth()->id()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @throws \Exception
     */
    public function dislike(Post $post)
    {
//        $post->like()->where('user_id', 1)->first()->delete();
        $post->like()->where('user_id', auth()->id())->first()->delete();
    }

}

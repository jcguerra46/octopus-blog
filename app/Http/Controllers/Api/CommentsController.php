<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CommentResource;
use App\Post;
use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentsController extends Controller
{
    /**
     * CommentsController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Post $post
     * @return mixed
     */
    public function index(Post $post)
    {
        return CommentResource::collection($post->comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Post $post
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Post $post, Request $request)
    {
        $comment = $post->comments()->create($request->all());
        return response()->json(new CommentResource($comment), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @param Comment $comment
     * @return CommentResource
     */
    public function show(Post $post, Comment $comment)
    {
        return new CommentResource($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Post $post
     * @param Request $request
     * @param Comment $comment
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Post $post, Request $request, Comment $comment)
    {
//        dd($request->all());
        $comment->update($request->all());
        return response()->json(new CommentResource($comment), Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @param Comment $comment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Post $post, Comment $comment)
    {
        $comment->delete();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}

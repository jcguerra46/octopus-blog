<?php

namespace App;

use App\User;
use App\Comment;
use App\Category;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Post extends Eloquent
{
    Const POST_PUBLISHED = 1;
    Const POST_NOT_PUBLISHED = 0;

    /**
     * Connection with mongodb
     *
     * @var string
     */
    protected $connection = 'mongodb';

    protected $collection = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'category_id',
        'title',
        'slug',
        'body',
        'sumary',
        'is_published'
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Relations belongsTo with User model
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Relation BelongsTo with Category Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Relations hasMany with Comment model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMAny(Comment::class);
    }

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
//     */
//    public function likes()
//    {
//        return $this->morphMany(Like::class, 'likeable');
////        return $this->hasMany(Like::class);
//    }

    public function like()
    {
        return $this->hasMany(Like::class);
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return asset("api/posts/$this->slug");
    }
}

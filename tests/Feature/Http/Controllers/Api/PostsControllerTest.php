<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Post;
use App\Http\Resources\PostResource;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
//use Tests\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class PostsControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    const URI = '/api/posts';

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory('App\User')->create();
        $this->category = factory('App\Category')->create();
        $this->post = factory('App\Post')->create([
            'user_id' => $this->user->id,
            'category_id' => $this->category->id,
            'is_published' => 0
        ]);
    }

    /** @test */
    public function non_authenticated_users_cannot_access_the_following_endpoints_for_the_post_api()
    {
        $endpoints = [
            ['action' => 'GET', 'path' => '/api/posts'],
            ['action' => 'POST', 'path' => '/api/posts'],
            ['action' => 'GET', 'path' => '/api/posts/-1'],
            ['action' => 'PUT', 'path' => '/api/posts/-1'],
            ['action' => 'DELETE', 'path' => '/api/posts/-1'],
        ];
        foreach($endpoints as $endpoint){
            $response = $this->json($endpoint['action'], $endpoint['path']);
            $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        }
    }

    /** @test */
    public function can_return_a_collection_of_paginate_posts()
    {
        factory('App\Post', 20)->create([
            'user_id' => $this->user->id,
            'category_id' => $this->category->id,
            'is_published' => 0
        ]);
        $response = $this->actingAs($this->user, 'api')->json('GET',self::URI);

        $response->assertStatus(Response::HTTP_OK)->assertJsonStructure([
            'data' => [
                '*' => [
                    '_id', 'author', 'category', 'title', 'slug', 'body', 'sumary', 'is_published', 'path', 'created_at'
                ]
            ],
            'links' => ['first', 'last','prev', 'next'],
            'meta' => ['current_page', 'from', 'last_page', 'path', 'per_page', 'to', 'total']
        ]);
    }

    /** @test */
    public function can_create_a_post()
    {
        $response = $this->actingAs($this->user, 'api')->json('POST',self::URI, [
            'user_id' => $userId = $this->user->id,
            'category_id' => $categoryId = $this->category->id,
            'title' => $title = $this->faker->sentence(6, true),
            'slug' => $slug = Str::slug($title, '-'),
            'body' => $body = $this->faker->paragraph(3, true),
            'sumary' => $sumary = $this->faker->sentence(12, true),
            'is_published' => $isPublished = 0,
        ]);
//\Log::info(1, [$response->getContent()]);
        $response->assertJsonStructure([
            '_id', 'user_id', 'category_id', 'title', 'slug', 'body', 'sumary', 'is_published', 'created_at'
        ])->assertJson([
            'user_id' => $userId,
            'category_id' => $categoryId,
            'title' => $title,
            'slug' => $slug,
            'body' => $body,
            'sumary' => $sumary,
            'is_published' => $isPublished
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('posts', [
            'user_id' => $userId,
            'category_id' => $categoryId,
            'title' => $title,
            'slug' => $slug,
            'body' => $body,
            'sumary' => $sumary,
            'is_published' => $isPublished
        ]);
    }

    /** @test */
    public function fail_with_a_404_if_post_not_found()
    {
        $response = $this->actingAs($this->user, 'api')->get(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_return_a_post()
    {
        $post = new PostResource($this->post);
        $response = $this->actingAs($this->user, 'api')->get( self::URI . "/$post->slug");
        $response->assertStatus(Response::HTTP_ACCEPTED)->assertExactJson([
            '_id' => $post->_id,
            'author' => $post->user->name,
            'category' => $post->category->name,
            'title' => $post->title,
            'slug' => $post->slug,
            'body' => $post->body,
            'sumary' => $post->sumary,
            'is_published' => $post->is_published,
            'path' => $post->path,
            'created_at' => (string)$post->created_at->diffForHumans()
        ]);

//        $user = factory('App\User')->create();
//        $post = factory('App\Post')->create(['user_id' => $user->id]);
//        $this->>actingAs($user, 'api')->get('/api/posts/' . $post->id)
//            ->assertSee($post->body);
    }

    /** @test */
    public function fail_with_404_if_post_we_want_to_updated_is_not_fount()
    {
        $response = $this->actingAs($this->user, 'api')->put(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_update_a_post()
    {
        $post = new PostResource($this->post);
        $response = $this->actingAs($this->user, 'api')->json('PUT',self::URI . "/$post->slug", [
            'user_id' => $this->user->id,
            'category_id' => $post->category_id,
            'title' => $post->title . '_updated',
            'slug' => $post->slug . '-updated',
            'body' => $post->body . ' UPDATED',
            'sumary' => $post->sumary . ' updated',
            'is_published' => $post->is_published
        ]);
        $response->assertStatus(Response::HTTP_OK)->assertExactJson([
            '_id' => $post->_id,
            'author' => $post->user->name,
            'category' => $post->category->name,
            'title' => $post->title . '_updated',
            'slug' => $post->slug . '-updated',
            'body' => $post->body . ' UPDATED',
            'sumary' => $post->sumary . ' updated',
            'is_published' => $post->is_published,
            'path' => $post->path . '-updated',
            'created_at' => (string)$post->created_at->diffForHumans()
        ]);
        $this->assertDatabaseHas('posts', [
            '_id' => $post->_id,
            'user_id' => $this->user->id,
            'category_id' => $post->category_id,
            'title' => $post->title . '_updated',
            'slug' => $post->slug . '-updated',
            'body' => $post->body . ' UPDATED',
            'sumary' => $post->sumary . ' updated',
            'is_published' => $post->is_published,
            'created_at' => (string)$post->created_at,
            'updated_at' => (string)$post->updated_at,
        ]);
    }

    /** @test */
    public function fail_with_404_if_post_we_want_to_delete_is_not_fount()
    {
        $response = $this->actingAs($this->user, 'api')->delete(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_delete_a_post()
    {
        $post = new PostResource($this->post);
        $response = $this->actingAs($this->user, 'api')->delete(self::URI . "/$post->slug");
        $response->assertStatus(Response::HTTP_NO_CONTENT)->assertSee(null);
        $this->assertDatabaseMissing('posts', ['slug' => $post->slug]);
    }
}
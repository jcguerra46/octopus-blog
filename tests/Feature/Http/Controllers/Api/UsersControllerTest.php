<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\User;
use Faker\Factory;
use Illuminate\Support\Str;
use App\Http\Resources\UserResource;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UsersControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    const URI = '/api/users';

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory('App\User')->create();
    }

    /** @test */
    public function non_authenticated_users_cannot_access_the_following_endpoints_for_the_user_api()
    {
        $endpoints = [
            ['action' => 'GET', 'path' => '/api/users'],
            ['action' => 'POST', 'path' => '/api/users'],
            ['action' => 'GET', 'path' => '/api/users/-1'],
            ['action' => 'PUT', 'path' => '/api/users/-1'],
            ['action' => 'DELETE', 'path' => '/api/users/-1'],
        ];
        foreach($endpoints as $endpoint){
            $response = $this->json($endpoint['action'], $endpoint['path']);
            $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        }
    }

    /** @test */
    public function can_create_a_user()
    {
        $response = $this->actingAs($this->user,'api')->post(self::URI, [
            'name' => $name = "Octopus User Test",
            'email' => $email = $this->faker->unique()->safeEmail, //"usertest@octopusblog.test", // $this->faker->unique()->safeEmail,
            'email_verified_at' => $emailVerifiedAt = now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'role_id' => $roleId = 3,
            'remember_token' => $this->generateRememberToken(10),
        ]);

        $response->assertJsonStructure([
            'id', 'name', 'email', 'role_id', 'created_at'
        ])->assertJson([
            'name' => $name,
            'email' => $email,
            'role_id' => $roleId
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('users', [
            'name' => $name,
            'email' => $email,
            'role_id' => $roleId
        ]);

    }

    /** @test */
    public function fail_with_a_404_if_user_not_found()
    {
        $response = $this->actingAs($this->user,'api')->get(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_return_a_user()
    {
        $user = new UserResource($this->user);
        $response = $this->actingAs($this->user,'api')->get( self::URI . "/$user->id");
        $response->assertStatus(Response::HTTP_ACCEPTED)->assertExactJson([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role_id' => $user->role_id,
            'email_verified_at' => (string)$user->email_verified_at,
            'created_at' => (string)$user->created_at,
            'updated_at' => (string)$user->updated_at,
        ]);
    }

    /** @test */
    public function fail_with_404_if_user_we_want_to_updated_is_not_fount()
    {
        $response = $this->actingAs($this->user,'api')->put(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_update_a_user()
    {
        $user = new UserResource($this->user);
        $response = $this->actingAs($this->user,'api')->put(self::URI . "/$user->id", [
            'name' => $user->name . ' UPDATED',
            'email' => $user->email,
            'role_id' => $user->role_id,
            'updated_at' => $updatedAt = (string)$user->updated_at
        ]);
//        \Log::info(1, [$response->getContent()]);
        $response->assertStatus(Response::HTTP_OK)->assertExactJson([
            'id' => $user->id,
            'name' => $user->name . ' UPDATED',
            'email' => $user->email,
            'email_verified_at' => (string)$user->email_verified_at,
            'role_id' => $user->role_id,
            'created_at' => (string)$user->created_at,
            'updated_at' => $updatedAt
        ]);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => $user->name . ' UPDATED',
            'email' => $user->email,
            'role_id' => $user->role_id,
            'updated_at' => $updatedAt,
            'created_at' => (string)$user->created_at,
        ]);
    }

    /** @test */
    public function fail_with_404_if_user_we_want_to_delete_is_not_fount()
    {
        $response = $this->actingAs($this->user,'api')->delete(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_delete_a_user()
    {
        $user = new UserResource($this->user);
        $response = $this->actingAs($this->user,'api')->delete(self::URI . "/$user->id");
        $response->assertStatus(Response::HTTP_NO_CONTENT)->assertSee(null);
        $this->assertDatabaseMissing('users', ['id' => $user->id]);
    }

    /** @test */
    public function can_return_a_collection_of_paginate_users()
    {
        factory('App\User', 15)->create();
        $response = $this->actingAs($this->user,'api')->get(self::URI);

        $response->assertStatus(Response::HTTP_OK)->assertJsonStructure([
            'data' => [
                '*' => [
                    'id', 'name', 'email', 'role_id', 'email_verified_at', 'created_at'
                ]
            ],
            'links' => ['first', 'last','prev', 'next'],
            'meta' => ['current_page', 'from', 'last_page', 'path', 'per_page', 'to', 'total']
        ]);
    }
}

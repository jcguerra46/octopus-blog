<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Support\Str;
use App\Http\Resources\CategoryResource;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CategoriesControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    const URI = '/api/categories';

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory('App\User')->create();
        $this->category = factory('App\Category')->create();
    }

    /** @test */
    public function non_authenticated_users_cannot_access_the_following_endpoints_for_the_category_api()
    {
        $endpoints = [
            ['action' => 'GET', 'path' => '/api/categories'],
            ['action' => 'POST', 'path' => '/api/categories'],
            ['action' => 'GET', 'path' => '/api/categories/-1'],
            ['action' => 'PUT', 'path' => '/api/categories/-1'],
            ['action' => 'DELETE', 'path' => '/api/categories/-1'],
        ];
        foreach($endpoints as $endpoint){
            $response = $this->json($endpoint['action'], $endpoint['path']);
            $response->assertStatus(401);
        }
    }

    /** @test */
    public function can_return_a_collection_of_paginate_categories()
    {
        $response = $this->actingAs($this->user,'api')->get(self::URI);

        $response->assertStatus(Response::HTTP_OK)->assertJsonStructure([
            'data' => [
                '*' => [
                    'id', 'name', 'description', 'created_at'
                ]
            ],
            'links' => ['first', 'last','prev', 'next'],
            'meta' => ['current_page', 'from', 'last_page', 'path', 'per_page', 'to', 'total']
        ]);
    }

    /** @test */
    public function can_create_a_categories()
    {
        $respnse = $this->actingAs($this->user,'api')->json('POST', self::URI, [
            'name' => $name = $this->faker->sentence(2, true),
            'description' => $description = $this->faker->sentence(8, true),
        ]);

        $respnse->assertJsonStructure([
            'id', 'name', 'description', 'created_at'
        ])->assertJson([
            'name' => $name,
            'description' => $description
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('categories', [
            'name' => $name,
            'description' => $description
        ]);
    }

    /** @test */
    public function fail_with_a_404_if_category_not_found()
    {
        $response = $this->actingAs($this->user,'api')->get(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_return_a_category()
    {
        $category = $this->category;
        $response = $this->actingAs($this->user,'api')->json('GET',self::URI . "/$category->slug");
        $response->assertStatus(Response::HTTP_ACCEPTED);
    }

    /** @test */
    public function fail_with_404_if_category_we_want_to_updated_is_not_fount()
    {
        $response = $this->actingAs($this->user,'api')->put(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_update_a_category()
    {
        $category = new CategoryResource($this->category);
        $response = $this->actingAs($this->user,'api')->put(self::URI . "/$category->slug", [
            'name' => $category->name . ' updated',
            'description' => $category->description . ' updated'
        ]);
        $response->assertStatus(Response::HTTP_OK)->assertExactJson([
            'id' => $category->id,
            'name' => $category->name . ' updated',
            'slug' => $category->slug . '-updated',
            'description' => $category->description . ' updated',
            'created_at' => (string)$category->created_at->diffForHumans()
        ]);
        $this->assertDatabaseHas('categories', [
            'id' => $category->id,
            'name' => $category->name . ' updated',
            'slug' => $category->slug . '-updated',
            'description' => $category->description . ' updated',
            'created_at' => (string)$category->created_at,
            'updated_at' => (string)$category->updated_at,
        ]);
    }

    /** @test */
    public function fail_with_404_if_category_we_want_to_delete_is_not_fount()
    {
        $response = $this->actingAs($this->user,'api')->delete(self::URI . '/-1');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function can_delete_a_category()
    {
        $category = new CategoryResource($this->category);
        $response = $this->actingAs($this->user,'api')->delete(self::URI . "/$category->slug");
        $response->assertStatus(Response::HTTP_NO_CONTENT)->assertSee(null);
        $this->assertDatabaseMissing('categories', ['id' => $category->id]);
    }
}

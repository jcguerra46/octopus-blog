<?php

namespace Tests\Feature\Http\Controllers\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('passport:install');
    }

    /** @test */
    public function can_authenticate()
    {
        $response = $this->post('/auth/token', [
//            'email' => factory('App\User')->create()->email,
            'email' => $this->create('User', [], false)->email,
            'password' => 'password'
        ]);
//        \Log::info(1, [$response->getContent()]);
        $response->assertStatus(Response::HTTP_OK)->assertJsonStructure(['token']);
    }
}

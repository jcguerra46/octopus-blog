<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->sentence(2, true),
        'slug' => Str::slug($name, '-'),
        'description' => $faker->sentence(8, true),
    ];
});

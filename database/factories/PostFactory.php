<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\User;
use App\Category;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'category_id' => Category::all()->random()->id,
        'title' => $title = $faker->sentence(6, true),
        'slug' => Str::slug($title, '-'),
        'body' => $faker->paragraph(4, true),
        'sumary' => $faker->sentence(12, true),
        'is_published' => $faker->randomElement([Post::POST_PUBLISHED, Post::POST_NOT_PUBLISHED]),
    ];
});

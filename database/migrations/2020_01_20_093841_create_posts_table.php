<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('posts', function ($collection) {
            $collection->bigIncrements('id')->index();
            $collection->bigInteger('user_id')->unsigned()->index();
            $collection->bigInteger('category_id')->unsigned()->index();
            $collection->string('title');
            $collection->string('slug')->index();
            $collection->text('body')->nullable();
            $collection->string('sumary')->nullable();
            $collection->bigInteger('is_published')->default(0);
            $collection->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mongodb')->drop('posts');
    }
}
